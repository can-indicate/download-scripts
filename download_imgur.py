#!/usr/bin/python3
"""
My personal script to download galleries from Imgur.


TODO:
- Use argparse for parsing commandline arguments.
"""
import re
import os
import sys
import requests as req
import json


def download_media(media: str, filename: str = None) -> None:
    """
    Download a binary file.

    Parameters:
    media[str]: the link to the media file.
    """
    r = req.get(media)
    filename = media.split("/")[-1] if filename is None else filename
    f = open(filename, mode='wb')
    for content in r.iter_content(131072):
        f.write(content)
    f.close()


def download_imgur_album(link: str) -> None:
    """
    Download an Imgur album, given the link to it.

    Parameters:
    link[str]: The link to the album.
    """
    link = link.strip('/')
    # Get the zip and check if it's valid.
    filename = link.split('/')[-1] + ".zip"
    link = link.replace('gallery', 'a') + '/zip'
    download_media(link, filename)
    is_zip = False
    with open(filename, "rb") as f:
        if f.read(4).hex() == '504b0304':
            is_zip = True
    if is_zip:
        return
    os.remove(filename)
    # If the zip link doesn't work, use embed json data
    link = link.replace('gallery', 'a') + '/embed'
    embed_page = str(req.get(link).content)
    matches = re.findall('\\{"hash":"[a-zA-Z0-9_\\-]+"[^\\{\\}]*'
                         + '"ext":"[\\.a-zA-Z]+"[^\\{\\}]*\\}', 
                         embed_page)
    matches = list({key: key for key in matches})
    im_list = [json.loads(e) for e in matches]
    for im in im_list:
        filename = im['hash'] + im['ext']
        link = 'https://i.imgur.com/' + filename
        download_media(link) 


if __name__ == "__main__":
    if len(sys.argv) == 2:
        link = sys.argv[1]
        download_imgur_album(link)
    else:
        sys.exit("Usage: python3 -m download_imgur /link/to/gallery")