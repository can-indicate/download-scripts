from hashlib import sha256
import glob
import os


def remove_duplicate_files_in_directory(directory_path: str):
    hash_set = set()
    files = glob.glob(f"{directory_path}/*")
    for filename in files:
        if not os.path.isdir(filename):
            h = sha256()
            with open(filename, 'rb') as file:
                # https://stackoverflow.com/a/55542529
                while chunk := file.read(h.block_size):
                    h.update(chunk)
            hexdigest = h.hexdigest()
            print(f"hash of '{filename}' is {hexdigest}.")
            if hexdigest in hash_set:
                print(f"removing '{filename}'.")
                os.remove(filename)
            else:
                hash_set.add(hexdigest)


if __name__ == "__main__":
    import sys
    directory_path = sys.argv[1]
    remove_duplicate_files_in_directory(directory_path)
