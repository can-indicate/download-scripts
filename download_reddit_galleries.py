from scrape_reddit import *


def download_links(page):
    links = get_reddit_gallery_links(page)
    for link in links:
        print(link)
        download_media(link)


if __name__ == "__main__":

    import sys
    link = sys.argv[1]
    dict_args = {}
    if len(sys.argv) > 2:
        for i in range(2, len(sys.argv)):
            arg_i = sys.argv[i]
            k, arg = arg_i.split('=')
            dict_args[k] = arg
    user_agent = get_user_agent()
    page_contents = download_reddit_page(link,
                                         user_agent, dict_args)
    page_contents_dict = {'contents': page_contents}
    download_links(page_contents)
    while(reddit_link := get_next_page(page_contents_dict['contents'])):
        page_contents = download_reddit_page(reddit_link,
                                             user_agent, dict_args)
        page_contents_dict['contents'] = page_contents
        download_links(page_contents)
        


