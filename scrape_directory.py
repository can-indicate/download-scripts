#!/usr/bin/python3
"""
Download files from a directory.

TODO:
- Add the ability to download only certain filetypes.
- The url encodings are actually not complete. Get all encodings.
- Need to deal with server disconects.
- Files with ':' (and probably some other characters) 
  appear to be skipped. This may be related to
  the above issue.
"""
from re import findall
import os
import asyncio
import requests as req
import aiohttp


def replace_encodings(filename: str) -> str:
    """
    Replace percentage encodings of a filename string with its character.
    """
    # url percent encoding reference:
    # https://developer.mozilla.org/en-US/docs/Glossary/percent-encoding
    url_encodings = [r"%3A", r"%2F", r"%3F", r"%23", r"%5B", r"%5D", r"%40",
                     r"%21", r"%24", r"%26", r"%27", r"%28", r"%29", r"%2A",
                     r"%2B", r"%2C", r"%3B", r"%3D",
                     r"%25", r"%20"]
    special_chars = [':', '/', '?', '#', '[', ']', '@', '!', '$', '&', "'",
                     '(', ')', '*', '+', ',', ';', '=', '%', ' ']
    for i in range(len(url_encodings)):
        if url_encodings[i] in filename:
            filename = filename.replace(url_encodings[i], special_chars[i])
    return filename


async def async_downloads(urls: list, subfolder: str) -> None:
    """
    Asynchronously get resources from a list of urls, and save the resources
    to local storage.
    """
    subfolder += '/' if subfolder and subfolder[-1] != '/' else ''
    async with aiohttp.ClientSession() as session:
        for media in urls:
            print(media)
            async with session.get(media) as response:
                filename = subfolder + media.split("/")[-1]
                filename = replace_encodings(filename)
                with open(filename, 'wb') as f:
                    while not response.content.at_eof():
                        data = await response.content.read()
                        f.write(data)


def scrape(link: str, r_level: int = 0, dir_name: str = '',
           dl_if_path_exists=True) -> None:
    """
    Download media files that are on the directory page.
    """
    page_source = req.get(link).text
    media_links_set = (set(findall('href="[^"/]*[a-zA-Z]*"', page_source))
                       - set(findall(r'href="\?[A-Z]=[A-Z];[A-Z]=[A-Z]"',
                                     page_source)))
    media_links = list(media_links_set)
    for i in range(len(media_links)):
        media_links[i] = media_links[i].replace('href=', '').replace('"', '')
        media_links[i].replace('href=', '').replace('"', '')
        media_links[i] = ((link + '/' + media_links[i]) if
                          media_links[i][-1] != '/' else
                          (link + media_links[i]))
    if not dl_if_path_exists:
        d = dir_name + ('/' if dir_name and dir_name[-1] != '/' else '')
        media_links = [m for m in media_links if not
                       os.path.exists(replace_encodings(d + m.split("/")[-1]))]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(async_downloads(media_links, dir_name))
    if r_level > 0:
        subdir_links = findall('href="[^"/(..)(.)]*/"', page_source)
        for i in range(len(subdir_links)):
            subdir_links[i] = subdir_links[i].replace(
                'href=', '').replace('"', '')
            subdir_name = dir_name + ('/' if dir_name and
                                      dir_name[-1] != '/' else ''
                                      ) + subdir_links[i]
            if subdir_name != '/' and subdir_name not in ['.', '..']:
                subdir_name = replace_encodings(subdir_name)
                go_into_subdir = True
                try:
                    os.mkdir(subdir_name)
                except FileExistsError as file_exists_error:
                    print(file_exists_error)
                    if not dl_if_path_exists:
                        go_into_subdir = False
                if go_into_subdir:
                    subdir_links[i] = (link + '/' + subdir_links[i]) if \
                                    link[-1] != '/' else \
                                    (link + subdir_links[i])
                    scrape(subdir_links[i], r_level-1, subdir_name)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        description='Download files from a directory.'
    )
    parser.add_argument(
        'link', metavar='https://link/to/directory',
        type=str,
        help='link to a directory')
    parser.add_argument(
        '-l',
        dest='level',  # This becomes an args attribute
        action='append',
        default=[],
        help='number of additional subdirectories to recurse into')
    parser.add_argument(
        '--skip-existing',
        action='store_true',
        help='download file or directory only if it does not exist'
    )
    ARGS = parser.parse_args()
    LEVEL = int(ARGS.level[0]) if ARGS.level else 0
    SKIP_EXISTING = ARGS.skip_existing
    scrape(ARGS.link, LEVEL, dl_if_path_exists=not SKIP_EXISTING)

