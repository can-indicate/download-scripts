#!/usr/bin/python3
"""
Script to automate downloading media files from reddit.
A user-agent.txt file containing a user agent string is
needed in order for this to work. An optional cookies.txt
for cookies and an ignore.txt file to exclude the download
of certain files may be supplied as well.

youtube-dl and ffmpeg are required to download youtube
and reddit videos. 

TODO:
 - Need to exclude ad videos
 - Get comment sections
 - Add options that only downloads or excludes
   images, videos, etc.
"""
import re
import os
import subprocess
import asyncio
try:
   import aiohttp
except ImportError:
    aiohttp = None
import requests as req
import json
from multiprocessing import Pool


async def async_downloads(urls: list) -> None:
    async with aiohttp.ClientSession() as session:
        for media in urls:
            print(media)
            async with session.get(media) as response:
                filename = media.split("/")[-1]
                with open(filename, 'wb') as f:
                    while not(response.content.at_eof()):
                        data = await response.content.read()
                        f.write(data)


def download_media(media: str, filename: str = None) -> None:
    """
    Download a binary file.

    Parameters:
    media[str]: the link to the media file.
    """
    r = req.get(media)
    filename = media.split("/")[-1] if filename is None else filename
    f = open(filename, mode='wb')
    for content in r.iter_content(2048):
        f.write(content)
    f.close()


def get_user_agent() -> str:
    """
    Return a user agent string from a user-agent.txt file.
    """
    user_agent = ''
    if os.path.exists('user-agent.txt'):
        with open('user-agent.txt', 'r') as f:
            for line in f:
                user_agent += line
        user_agent = user_agent.strip()
    return user_agent


def get_ignore_list() -> 'list[str]':
   """
   Get from an ignore.txt file a list of patterns used for
   excluding files. 
   """
   ignore_list = []
   if os.path.exists('ignore.txt'):
      with open('ignore.txt', 'r') as f:
        for unfiltered_line in f:
            line = unfiltered_line.strip('\n').strip(' ')
            if line != '':
                ignore_list.append(line)
        if len(ignore_list) > 0:
            return ignore_list
   return None


def get_cookies() -> dict:
    """
    Return a cookies dictionary from a cookies.txt file.
    """
    try:
        f = open('cookies.txt', 'r')
    except FileNotFoundError:
        return {}
    cookies = json.load(f)
    f.close()
    return cookies


def download_reddit_page(page: str, user_agent: str = "", 
                         cookies: dict = {}) -> str:
    """
    Return a Reddit page.

    Parameters:
    page[str]: link to the reddit page.
    user_agent[str]: user agent string.
    cookies[dict]: cookies.
    """
    r = req.get(page, headers={'User-agent': user_agent},
                cookies=cookies
                ) if user_agent != '' else req.get(page)
    return str(r.content)


def get_reddit_gallery_links(page_content: str) -> 'list[str]':
    """
    Download images from reddit galleries.

    Parameters:
    page_content[str]: the page contents

    Returns:
    List of unique media links
    """
    image_id_list = []
    gallery_pattern_string = "data\-media\-ids=&quot\;[^;]+\;"
    image_id_pattern_string = "[a-z0-9]{6,}"
    gallery_pattern = re.compile(gallery_pattern_string)
    image_id_pattern = re.compile(image_id_pattern_string)
    gallery_matches = gallery_pattern.findall(page_content)
    for m in gallery_matches:
        image_id_matches = image_id_pattern.findall(str(m))
        for image_id in image_id_matches:
            image_id_list.append(f"https://i.redd.it/{str(image_id)}.jpg")
    if image_id_list == []:
        image_id_pattern_string2 = 'data\-media\-id=\"[a-z0-9]{6,}\"'
        image_id_pattern = re.compile(image_id_pattern_string2)
        image_matches = image_id_pattern.findall(page_content)
        image_id_set = set()
        for m in image_matches:
            image_id = str(m).split('=')[-1].strip('"')
            image_id_set.add(f"https://i.redd.it/{image_id}.jpg")
        for m in image_id_set:
            image_id_list.append(m)
    return image_id_list


def get_downloadable_links(page_content: str,
                           ignore_list: list = None) -> 'list[str]':
    """
    Get the directly downloadable media in a page.

    Parameters:
    page_content[str]: the page contents

    Returns:
    List of unique media links
    """
    url_start = "(?:www|http|https):/{1,2}"
    sites_list = ["(?:i\\.)?imgur\\.com", 
                  "i\\.redd\\.it", 
                  "cdn[a-z0-9][0-9]?\\.artstation.com(?:/[a-zA-Z0-9_\\-]*)*",
                  "upload\\.wikimedia\\.org(?:/[a-zA-Z0-9_\\-]*)*", 
                  "[0-9]{2}\\.media.tumblr.com/[a-zA-Z0-9_\\-]*"
                 ]
    sites = '(?:' + '|'.join(sites_list) + ')/{1,2}[^\"/\\s]+'
    endings = "\\.(?:png|gifv|gif|jpg|jpeg|PNG|GIF|JPG|JPEG|GIFV|GIF)(?:\\?[0-9]+)?"
    pattern_string = url_start + sites + endings
    pattern = re.compile(pattern_string)
    matches = pattern.findall(page_content)
    for i in range(len(matches)):
        if matches[i].endswith('gifv'):
            matches[i] = matches[i].replace('gifv', 'mp4')
    links = list(set(matches))
    links.extend(get_reddit_gallery_links(page_content))
    if ignore_list is not None:
        filtered_links = []
        for link in links:
            if not any([re.search(pattern_string, link)
                        for pattern_string in ignore_list]):
                filtered_links.append(link)
        return filtered_links
    return links


def get_external_links(page_content: str,
                       ignore_list: list = None) -> 'dict[str, list[str]]':
    """
    Get the links to pages that host downloadable media.

    Parameters:
    page_content[str]: The contents of the page.

    Returns a dictionary where the keys describe the type of media and
    where they're from and values are the list of links.
    """
    start = "https?://"
    media_sites = {
        'youtubeVideos': start + "www\\.youtube\\.com/watch\\?v=[a-zA-Z0-9_\\-]{11}",
        'arxiv': start + "arxiv\\.org/abs/[0-9\\.]+",
        'redditVideos': start + "v\\.redd\\.it/[a-z0-9_\\-]+",
        'gfycatVideos': f'"{start}(?:www\\.)?gfycat\\.com/[a-zA-Z]+"',
        'redgifsVideos': f'"{start}(?:www\\.)?redgifs\\.com/watch/[a-zA-Z]+"',
        'imgurAlbums': f'"{start}(?:i\\.)?imgur\\.com/(?:gallery/|a/)?[a-zA-Z0-9_]+"'
    }
    links = {}
    for key in media_sites.keys():
        matches = re.compile(media_sites[key]).findall(page_content)
        matches = list(set(matches))
        for i in range(len(matches)):
            matches[i] = matches[i].strip('"') 
            if matches[i].startswith("http:"):
                matches[i] = matches[i].replace("http:", "https:")
        if ignore_list is not None:
            filtered_matches = []
            for m in matches:
                if not any([re.search(pattern_string, m)
                            for pattern_string in ignore_list]):
                    filtered_matches.append(m)
            links[key] = filtered_matches
        else:
            links[key] = matches
    return links


def get_comments(page_content: str) -> 'list[str]':
    """
    Get links to the comments.

    Parameters:
    page_content[str]: the page contents

    Returns a list of links to the comments.
    """
    matches = re.findall('comments(?:/[^/"]+){2}/"', page_content)
    #TODO: this only works in subreddits
    subreddit = re.findall("/r/[a-zA-Z_\\-]+/post/login", page_content)[0]
    subreddit = subreddit.replace("/post/login", "").replace("/r/", "")
    # print(subreddit)
    comments = [f'https://old.reddit.com/r/{subreddit}/' + m.strip('"') 
                for m in list(set(matches))]
    return comments


def get_next_page(page_content: str) -> str:
    """
    Get the next page.

    Parameters:
    page_content[str]: the page contents

    Returns the link to the next page.
    """
    pattern = re.compile('"[^"]+count=[0-9]+&amp;after=[a-z_A-Z0-9]+"')
    match = pattern.findall(page_content)
    matches = [m.strip('"') for m in list(set(match))]
    if matches == []:
        return ''
    elif len(matches) == 1:
        return matches[0]
    else:
        return matches[1]


def download_reddit_video(link) -> None:
    """
    Download a Reddit video, given the link to it.

    Parameters:
    link[str]: The link to the video.
    """
    link += '/' if link[-1] != '/' else ''
    mpd = link + "DASHPlaylist.mpd"
    r = req.get(mpd)
    mpd = ''.join([str(content) for content in r.iter_content(1024)])
    dashes = list(set(re.findall('DASH_(?:audio|[0-9]{3})\\.mp4', mpd)))
    if dashes == []:
        return
    dashes.sort()
    audio, video = (dashes[-1], dashes[-2]) if 'DASH_audio.mp4' in dashes else (
                    None, dashes[-1]) 
    video = link + video
    filename = link.split("/")[-2] + ".mp4"
    if audio is not None:
        audio = link + audio
        download_media(video)
        download_media(audio)
        # Using ffmpeg to merge video-only and audio-only files:
        # https://superuser.com/a/277667
        # Credit to [patrick-georgi](https://superuser.com/users/76365/patrick-georgi)
        # edited by [llogan](https://superuser.com/users/110524/llogan)
        # Original question: https://superuser.com/q/277642
        # by [Sandy](https://superuser.com/users/48164/sandy)
        # edited by [bummi](https://superuser.com/users/172747/bummi)
        try:
            subprocess.call(['ffmpeg', '-i', video, '-i', audio, 
                            '-c', 'copy', filename.replace(".mp4", ".mkv")])
            os.remove(video.split("/")[-1])
            os.remove(audio.split("/")[-1])
        except FileNotFoundError as e:
            print(e)
    else:
        download_media(video, filename=filename)


def download_youtube_video(link: str) -> None:
    """
    Download a Youtube video, given the link to it.
    This requires youtube-dl in order for it to work.

    Parameters:
    link[str]: The link to the video.
    """
    try:
        subprocess.call(["youtube-dl", link])
    except FileNotFoundError as e:
        print(e)


def download_imgur_album(link: str) -> None:
    """
    Download an Imgur album, given the link to it.

    Parameters:
    link[str]: The link to the album.
    """
    link = link.strip('/')
    # Get the zip and check if it's valid.
    filename = link.split('/')[-1] + ".zip"
    link = link.replace('gallery', 'a') + '/zip'
    download_media(link, filename)
    with open(filename, "rb") as f:
        is_zip = f.read(4).hex() == '504b0304'
    if is_zip:
        return
    os.remove(filename)
    # If the zip link doesn't work, use embed json data
    link = link.replace('gallery', 'a') + '/embed'
    embed_page = str(req.get(link).content)
    matches = re.findall('\\{"hash":"[a-zA-Z0-9_\\-]+"[^\\{\\}]*'
                         + '"ext":"[\\.a-zA-Z]+"[^\\{\\}]*\\}', 
                         embed_page)
    matches = list({key: key for key in matches})
    im_list = [json.loads(e) for e in matches]
    for im in im_list:
        filename = im['hash'] + im['ext']
        link = 'https://i.imgur.com/' + filename
        download_media(link) 


def get_arxiv_document_file_link(link: str) -> None:
    """
    Given the link to the Arxiv page, get a direct link
    to the arxiv pdf document.
    """
    link = link.strip('/')
    return link.replace('abs', 'pdf') + '.pdf'


def download_arxiv_document(link: str) -> None:
    """
    Download an Arxiv pdf document, given the link to its page.

    Parameters:
    link[str]: The link to the Arxiv page.
    """
    download_media(get_arxiv_document_file_link(link))


def get_gfycat_video_file_link(link: str) -> None:
    """
    Given a link to a gfycat page, get the 
    link to the true video file.

    Parameters:
    link[str]: The link to the video page.
    """
    r = req.get(link)
    page = ''.join([str(content) for content in r.iter_content(1024)])
    matches = re.findall("[a-zA-Z_\\-]*\\-mobile\\.mp4", page)
    downloads = set([m for m in matches if 
                     m.lower().find(link.split('/')[-1].lower()) == 0])
    if len(downloads) == 0:
        return
    return 'https://thumbs.gfycat.com/' + max(downloads)


def download_gfycat_video(link: str) -> None:
    """
    Download a Gfycat video, given the link to it.

    Parameters:
    link[str]: The link to the video.
    """
    download_media(get_gfycat_video_file_link(link))


def get_redgifs_video_file_link(link: str) -> None:
    """
    Given a link to a redgifs page, get the 
    link to the true video file.

    Parameters:
    link[str]: The link to the video page.
    """
    r = req.get(link)
    page = ''.join([str(content) for content in r.iter_content(1024)])
    matches = re.findall("[a-zA-Z_\\-]*\\-mobile\\.mp4", page)
    downloads = set([m for m in matches if 
                     m.lower().find(link.split('/')[-1].lower()) == 0])
    if len(downloads) == 0:
        return
    return 'https://thumbs2.redgifs.com/' + max(downloads)


def download_redgifs_video(link: str) -> None:
    """
    Download a Redgifs video, given the link to it.

    Parameters:
    link[str]: The link to the video.
    """
    try:
        subprocess.call(["yt-dlp", link])
    except FileNotFoundError as e:
        print(e)
    # download_media(get_redgifs_video_file_link(link))


def write_page_file(page_contents):
    with open('page.html', 'w') as f:
        f.write(page_contents.strip('b').strip("'").replace(
                ">", ">\n").replace(";", ";\n").replace("\\'", "'").replace(
                    "{", "{\n").replace("}", "}\n"))


if __name__ == "__main__":

    # import sys
    import argparse
    # from time import perf_counter

    # t1 = perf_counter()

    links = set()

    direct_links_only = False
    scrape_in_comments = False
    videos_only = False
    def download(reddit_link, download_info):
        user_agent = download_info['user_agent']
        cookies = download_info['cookies']
        ignore_list = download_info['ignore_list']
        page_content = download_reddit_page(reddit_link, 
                                            user_agent, cookies)
        contents = get_downloadable_links(page_content, 
                                          ignore_list=ignore_list)
        print(reddit_link)
        write_page_file(page_content)
        ext_links = get_external_links(page_content, ignore_list=ignore_list)
        if videos_only:
            contents = [c for c in contents if
                        (c.endswith('mp4') or c.endswith('webm'))]
        contents = [c for c in contents if c not in links]
        for c in contents:
            links.add(c)
        if aiohttp is not None:
            loop = asyncio.get_event_loop()
            loop.run_until_complete(async_downloads(contents))
        else:
            for content in contents:
                print(content)
                download_media(content)
        if not direct_links_only:
            # external links download
            ext_links_dl = {'arxiv': download_arxiv_document,
                            'imgurAlbums': download_imgur_album,
                            'youtubeVideos': download_youtube_video,
                            'redditVideos': download_reddit_video,
                            'gfycatVideos': download_gfycat_video,
                            'redgifsVideos': download_redgifs_video,
                            }
            # external links multiprocessing download
            ext_links_mp_dl = {
                'redditVideos': download_reddit_video,
                'imgurAlbums': download_imgur_album,
                'youtubeVideos': download_youtube_video,
            }
            # external links get file
            ext_links_get_file = {
                'arxiv': get_arxiv_document_file_link,
                'gfycatVideos': get_gfycat_video_file_link,
                # 'redgifsVideos': get_redgifs_video_file_link
            }
            keys_to_remove = []
            if videos_only:
                for key in ext_links_dl:
                    if not key.endswith('Videos'):
                        keys_to_remove.append(key)
            for key in keys_to_remove:
                ext_links_get_file.pop(key)
                ext_links_mp_dl.pop(key)
                ext_links_dl.pop(key)
            async_dl_list = []
            pool_dl_list = []
            for key in ext_links_dl.keys():
                if key in ext_links_get_file and aiohttp:
                    # for pages that contain only a single file
                    # that are grouped into this key, this may
                    # actually be slower.
                    dl_links = [l for l in  ext_links[key] if 
                                l not in links]
                    dl_links = [ext_links_get_file[key](l) 
                                    for l in dl_links]
                    async_dl_list.extend(dl_links)
                elif key in ext_links_mp_dl:
                    dl_links = [[key, l] for l in  ext_links[key] if 
                                l not in links]
                    pool_dl_list.extend(dl_links)
                else:
                    for page in ext_links[key]:
                        if page not in links:
                            print(page)
                            ext_links_dl[key](page)
                            links.add(page)
            if pool_dl_list:
                with Pool(processes=4) as pool:
                    results = [(e[1], pool.apply_async(ext_links_dl[e[0]], (e[1], ))) 
                                for e in pool_dl_list]
                    for r in results:
                        print(r[0])
                        r[1].get()
                for c in pool_dl_list:
                    links.add(c[1])
            if async_dl_list:
                loop = asyncio.get_event_loop()
                loop.run_until_complete(async_downloads(async_dl_list))
                for c in async_dl_list:
                    links.add(c)
        if scrape_in_comments:
            comments = get_comments(page_content)
            for c in comments:
                if c not in links:
                    links.add(c)
                    download(c, download_info)
        return page_content


    parser = argparse.ArgumentParser(
        description="Scrape media files from Reddit."
    )
    parser.add_argument(
        'link', metavar='https://old.reddit.com/r/subreddit/page',
        type=str, help='Link to Reddit page.'
    )
    parser.add_argument(
        '--direct-links-only', action='store_true',
        help='only download directly downloadable media'
    )
    parser.add_argument(
        '--videos-only', action='store_true',
        help='only scrape video links'
    )
    parser.add_argument(
        '--scrape-in-comments', action='store_true',
        help='go into the comment sections'
    )
    parser.add_argument(
        '-l', 
        dest='level',  # This becomes an args attribute
        action='append', 
        default=[],
        help='number of additional pages to visit')


    args = parser.parse_args()
    reddit_link = args.link
    reddit_link = reddit_link.strip()
    reddit_link = re.sub('^old\\.reddit\\.com', r'https://old.reddit.com', 
                         reddit_link)
    reddit_link = re.sub('^(?:https?://?)?(?:www\\.)?reddit\\.com', 
                         r'https://old.reddit.com', reddit_link)
    direct_links_only = args.direct_links_only
    videos_only = args.videos_only
    scrape_in_comments = args.scrape_in_comments
    level = int(args.level[0]) if len(args.level) > 0 else 999
    user_agent = get_user_agent()
    cookies = get_cookies()
    ignore_list = get_ignore_list()
    download_info = {'user_agent': user_agent, 'cookies': cookies, 
                     'ignore_list': ignore_list}
    page_content = download(reddit_link, download_info)
    while(reddit_link := get_next_page(page_content)):
        if level == 0:
            break
        page_content = download(reddit_link, download_info)
        level -= 1

    # print(perf_counter() - t1)
